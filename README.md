**MVC Web API with Basic Crud Operations**

This MVC application help you provide basic Add,update,delete Operation using Web API

Requirment to Run Project: Visual Studio 2017 or later
Database: MySQL

API Methods:

**To Insert Data**
 /api/values/insertdata?name=abc&nic=123&cell=123

where:
/api  		defines API
/values 	define controller
/insertdata	define method
name,nic,cell are parameters
 
---------------------------------------------------------------

**To Update Data** 
 /api/values/updatedata?id=1&name=abc&nic=123&cell=123

where:
/api  		defines API
/values 	define controller
/updatedata	define method
id defines the id of record (of which data needs to be updated)
name,nic,cell are parameters


---------------------------------------------------------------

**To Delete Data**
 /api/values/delete?id=1

where:
/api  		defines API
/values 	define controller
/delete		define method
id define id of reccord (id of record which needs to be deleted
 
---------------------------------------------------------------

**To Reterive All Data** 
 /api/values/showdata

where:
/api  		defines API
/values 	define controller
/showdata	define method


---------------------------------------------------------------



**To Reterive Data based on id** 
 /api/values/showdata?id=1

where:
/api  		defines API
/values 	define controller
/showdata	define method
id  defines to fetch specific record


---------------------------------------------------------------