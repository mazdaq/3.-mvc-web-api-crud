﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;

namespace WebAPIT3.Controllers
{
    public class ValuesController : ApiController
    {
        //Passing Connection String to MySQL Connection
        MySqlConnection connection = new MySqlConnection("server=localhost;database=usermgt;port=3306;username=root;password=root2");

        //Mysql command
        MySqlCommand command;

        //Data Reader for reading rows and record
        MySqlDataReader reader;


        [HttpGet] //Insert data
        public string InsertData(string name, string nic, string cell)
        {
            //Insert data via API to database
            string insertQuery = "INSERT INTO user_info(name,nic,cell) VALUES('" + name + "','" + nic + "','" + cell + "')";
            
            //Execute Query
            executeMyQuery(insertQuery);

            return "data inserted";
        }


        [HttpGet] //Update Data 
        public string UpdateData(int id,string name,string nic, string cell)
        {
            //update data via API into database
            string updateQuery = "UPDATE user_info SET name='" + name + "', nic='" + nic+"',cell='"+cell+"' WHERE id="+id+" ";

            //Execute Query
            executeMyQuery(updateQuery);

            return "data updated";
        }


        [HttpGet]//Delete Data
        public void delete(int id)
        {
            //Delete data via API from database
            string query="Delete from user_info where id='"+id+"' ";
            executeMyQuery(query);
        }

        [HttpGet] //Show Data
        public string ShowData()
        {
            //Creating Commands
            command = connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Select query
            command.CommandText = "select * from user_info";

            //Open Connection
            openConnection(); 
            reader = command.ExecuteReader(); //Execute Reader reads row from database

            string str=""; //for storing feteched data
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //fetched recrod from database is stored in this string "str"
                    str += Convert.ToString(reader.GetInt32(0)) + "," + reader.GetString(1) + "," + reader.GetString(2) + "," + reader.GetString(3) ;
                }
            }

            closeConnection(); //close connection

            return str; //return and display data stored in string

        }


        [HttpGet] //Show data View ID
        public string ShowData(int id)
        {
            //Create command
            command = connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            //Select User based on ID parameter
            command.CommandText = "select * from user_info where id="+id+"";

            //open connection
            openConnection();

            //execute reader
            reader = command.ExecuteReader();

            //string to store data if there is any
            string str = "";


            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    str += Convert.ToString(reader.GetInt32(0)) + "," + reader.GetString(1) + "," + reader.GetString(2) + "," + reader.GetString(3);
                }
            }

            closeConnection(); //close connection

            return str; // return feteched user if there is any

        }


        

        //Open connection
        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }


        //close conneciton
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }



        //Helper for executing Non Queries
        public void executeMyQuery(string query)
        {
            try
            {
                //Open Connection
                openConnection();
                command = new MySqlCommand(query, connection);

                if (command.ExecuteNonQuery() == 1)
                {
                   
                }

                else
                {
                    
                }

            }
            catch (Exception ex)
            {
                //Catch Exception if there is any
            }
            finally
            {
                closeConnection(); //close connection
            }
        }
    }
}
